package hw3_18001087.test;

import java.util.Scanner;
import hw3_18001087.src.*;

class ComplexTest {
    static Scanner sc = new Scanner(System.in);

    public static Complex[] input(int n) {

        Complex[] comArr = new Complex[n];
        for (int i = 0; i < comArr.length; i++) {
            System.out.print("Input real part: ");
            float r = sc.nextFloat();
            System.out.print("Input image part: ");
            float img = sc.nextFloat();

            comArr[i] = new Complex(r, img);
        }

        return comArr;

    }

    public static void print(Complex[] a, int index) {
        if (index >= a.length) {
            System.out.println("Index excced array length");
        } else {
            for (int i = 0; i < a.length; i++) {
                if (i == index) {
                    System.out.println(a[i].toString());
                    break;
                }
            }
        }
    }

    public static Complex sum(Complex[] a) {
        Complex sumArr = new Complex(0, 0);

        for (int i = 0; i < a.length; i++) {
            sumArr = a[i].add(sumArr);
        }
        return sumArr;
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(sc.nextLine());
        Complex[] a = input(n);

        print(a, 2);

        System.out.println(sum(a).toString());
    }
}