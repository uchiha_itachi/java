package hw3_18001087.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import hw3_18001087.src.*;

public class WordCountTest {

    public static String openFile(String fileName){
        String text = "";
        Scanner sc;
        try {
            sc = new Scanner(new File(fileName));
            while(sc.hasNextLine()){
                text += sc.nextLine();
            }
        sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static SimpleArrayList<WordCount> count(String text){
        SimpleArrayList<WordCount> wc = new SimpleArrayList<>();
        Pattern pattern = Pattern.compile("\\w+");
        Matcher matcher = pattern.matcher(text);

        while(matcher.find()){
            String word = matcher.group();
            WordCount wordObj = new WordCount(word);
            if(wc.isContain(wordObj)){
                wordObj = wc.get(wordObj);
                wordObj.setCount(wordObj.getCount() + 1);
            }
            else {
                wc.add(wordObj);
            }
        }

        return wc;
    }
    public static void main(String[] args) {
        String text = openFile("test.txt");
        SimpleArrayList<WordCount> wcs = count(text);
        for (WordCount wordCount : wcs) {
            System.out.println(wordCount.toString());
        }
    }

}