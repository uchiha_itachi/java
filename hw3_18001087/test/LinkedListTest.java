package hw3_18001087.test;
import hw3_18001087.src.*;

public class LinkedListTest{

    
    public static void main(String[] args) {
        SimpleLinkedList<Integer> a = new SimpleLinkedList<>();
        a.add(1);
        a.add(2);
        System.out.println(a.getTop());
        a.printList();
        
        a.set(1, 10);
        a.printList();
    }
}