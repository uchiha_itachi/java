package hw3_18001087.test;
import hw3_18001087.src.*;
import java.util.Scanner;

class ShapeTest {

    static Scanner sc = new Scanner(System.in);

    public static Shape[] input(int n) {
        Shape[] shapeArr = new Shape[n];
        System.out.println("Option:");
        System.out.println("1. Input Circle.");
        System.out.println("2. Input Sphere.");
        System.out.println("3. Input Rectangle.");
        for (int i = 0; i < shapeArr.length; i++) {
            System.out.print("Your option: ");
            int k = Integer.parseInt(sc.nextLine());
            if (k == 1) {
                System.out.print("Input radius: ");
                double r = Double.parseDouble(sc.nextLine());
                shapeArr[i] = new Circle(r);
            }
            if (k == 2) {
                System.out.print("Input radius: ");
                double r = Double.parseDouble(sc.nextLine());
                shapeArr[i] = new Sphere(r);
            }
            if (k == 3) {
                System.out.print("Input width: ");
                double w = Double.parseDouble(sc.nextLine());
                System.out.print("Input height: ");
                double h = Double.parseDouble(sc.nextLine());

                shapeArr[i] = new Rectangle(w, h);
            }
        }
        return shapeArr;
    }

    public static void print(Shape [] a){
        for (int i = 0; i < a.length; i++) {
           System.out.println(a[i].toString());
        }
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(sc.nextLine());
        Shape [] a = input(n);
        print(a);
    }
}