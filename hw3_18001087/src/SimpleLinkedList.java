package hw3_18001087.src;

public class SimpleLinkedList<T> {
    class Node {
        T data;
        Node next;
    }

    private Node top = null;
    private Node bot = null;
    private int n = 0;

    /**
     * @return the bot data
     */
    public T getBot() {
        return bot.data;
    }

    /**
     * @return the top data
     */
    public T getTop() {
        return top.data;
    }

    // Add to top of list.
    public void add(T data) {
        Node t = new Node();
        t.data = data;
        t.next = top;
        if (top == null) {
            bot = t;
        }
        top = t;
        n++;
    }

    // Add to bottom of list.
    public void addBot(T data) {
        Node t = new Node();
        t.data = data;
        t.next = null;
        if (n == 0) {
            bot = t;
            top = t;
            n++;
        } else {
            bot.next = t;
            bot = t;
            n++;
        }
    }

    // Get data of element i.
    public T get(int i) {
        if (i >= n) {
            System.out.println("Index " + i + " is over size of list: " + n);
            return null;
        }
        int k = 0;
        Node t = top;
        while (k != i) {
            t = t.next;
            k++;
        }
        return t.data;
    }

    // Set data of element i.
    public void set(int i, T data) {
        if (i >= n) {
            System.out.println("Index " + i + " is over size of list: " + n);
        } else {
            int k = 0;
            Node t = top;
            while (k < i) {
                t = t.next;
                k++;
            }
            t.data = data;
        }

    }

    // Return "true" if list contain data.
    public boolean isContain(T data) {
        Node t = top;
        while (t != null) {
            if (t.data.equals(data)) {
                return true;
            }
            t = t.next;
        }
        return false;
    }

    // Return size of list.
    public int size() {
        return n;
    }

    // Return "true" if list is empty.
    public boolean isEmpty() {
        return n == 0;
    }

    // Remove top element.
    public T removeTop() {
        if (top == null) {
            return null;
        } else {
            T data = top.data;
            top = top.next;
            n--;
            return data;
        }

    }

    // Remove bottom element.
    public T removeBot() {
        bot = top;
        while (bot.next.next != null) {
            bot = bot.next;
        }
        T data = bot.next.data;
        bot.next = null;
        n--;
        return data;
    }

    // Remove all element equal data.
    public void remove(T data) {
        while (top.data .equals(data)){
            removeTop();
        }
        while (bot.data.equals(data)) {
            removeBot();
        }
        if (n != 0) {
            Node t = top;
            while (t.next != null) {
                if (t.next.data.equals(data)) {
                    t.next = t.next.next;
                    n--;
                }
                t = t.next;
            }
        }
    }

    // Print all element of list.
    public void printList() {
        Node t = top;
        while (t != null) {
            System.out.print(t.data + " ");
            t = t.next;
        }
        System.out.println();
    }
}
