package hw3_18001087.src;

public class Sphere extends Shape {

	private double radius = 0;

	public Sphere(double radius) {
		this.radius = radius;
	}

	@Override
	protected double getVolume() {
		return (4 / 3) * radius * radius * radius * Math.PI;
	}

	@Override
	protected double getArea() {
		return 4 * Math.PI * radius * radius;
	}

	@Override
	protected double getPerimeter() {
		return 0;
	}

	@Override
	public String toString() {
		return "Sphere [ Radius: " + radius + ", Area: " + getArea() +", Volume: "+ getVolume() +  " ]" ;
	}
}
