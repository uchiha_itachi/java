package hw3_18001087.src;
public class Rectangle extends Shape {

	private double width = 0;
	private double height = 0;
	
	public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
	}
	
	@Override
	protected double getVolume() {
		return 0;
	}

	@Override
	protected double getArea() {
        return width*height;
	}

	@Override
	protected double getPerimeter() {
		return 2*(width + height);
	}

	@Override
	public String toString() {
		return "Rectangle [ Width: " + width + ", Height: "+ height + ", Perimeter: " + getPerimeter() + ", Area: " + getArea() + " ]" ;
	}
}
