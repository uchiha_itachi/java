package hw3_18001087.src;
public class Circle extends Shape {

	private double radius = 0;
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	@Override
	protected double getVolume() {
		return 0;
	}

	@Override
	protected double getArea() {
		return radius*radius*Math.PI;
	}

	@Override
	protected double getPerimeter() {
		return 2*Math.PI*radius;
	}

	@Override
	public String toString() {
		return "Circle [ Radius: " + radius + ", Perimeter: " + getPerimeter() + ", Area: " + getArea() + " ]" ;
	}
  }
