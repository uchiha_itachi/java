package hw3_18001087.src;
public class Complex {
	
	private float real;  //phần thực
	private float image; //phần ảo 
	
	public Complex(float r, float i) {
        this.real = r;
        this.image = i;
	}
	
	public Complex add(Complex c) {
        float i = this.image + c.imagepart();
        float r = this.real + c.realpart();

        return new Complex(r, i);
	}
	
	public Complex minus(Complex c) {
        // Trừ số phức hiện tại cho số phức c
        float i = this.image - c.imagepart();
        float r = this.real - c.realpart();

        return new Complex(r, i);
	}
	
	public Complex time(Complex c) {
        // Nhân số phức hiện tại với số phức c
        float r = this.real * c.realpart() - this.image * c.imagepart();
        float i = this.real * c.imagepart() + this.image * c.realpart();

        return new Complex(r, i);
	}
	
	public float realpart() {
		return real;
	}
	
	public float imagepart() {
		return image;
	}
	
	@Override
	public String toString() {
        // In ra số phức
        return "Complex: " + this.real + " " + this.image + "i";		
	}		
}
