package hw3_18001087.src;

import java.util.Arrays;
import java.util.Iterator;

public class SimpleArrayList<T> implements ListInterface<T> {

    private T[] array;
    private int n = 0;
    private int defaultSize = 100;

    // Generate a list with defaul size.
    @SuppressWarnings("unchecked")
    public SimpleArrayList() {
        array = (T[]) new Object[defaultSize];
    }

    // Generate a list with given capacity.
    @SuppressWarnings("unchecked")
    public SimpleArrayList(int capacity) {
        this.array = (T[]) new Object[capacity];
    }

    // Return a iterator.
    @Override
    public Iterator<T> iterator() {
        Iterator<T> it = new Iterator<T>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return (currentIndex < n) && array[currentIndex] != null;
            }

            @Override
            public T next() {
                //if (hasNext()) {
                    return array[currentIndex++];
                // }
                // return null;

            }
        };
        return it;
    }

    // Add a element to bottom of list.
    @Override
    public void add(T data) {
        if (n < array.length) {
            array[n] = data;
            n++;
        }
        else{
            array = Arrays.copyOf(array, n+1);
            array[n] = data;
            n++;
        }
    }

    // Get value of element i.
    @Override
    public T get(int i) {
        if (i >= n) {
            System.out.println("Index: " + i + "is over of size: " + n);
            return null;
        }
        return array[i];
    }

    public T get(T data) {
        for (int i = 0; i < n; i++) {
            if (data.equals(array[i])) {
                return array[i];
            }
        }
        return null;
    }

    // Set value of element i.
    @Override
    public void set(int i, T data) {
        if (i < n) {
            array[i] = data;
        } else {
            System.out.println("Index: " + i + "is over of size: " + n);
        }
    }

    // Remove element with value of "data".
    @Override
    public void remove(T data) {
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != data) {
                array[j] = array[i];
                j++;
            } else {
                n--;
            }
        }
        array = Arrays.copyOf(array, n);
    }

    // Return "true" if list contains "data".
    @Override
    public boolean isContain(T data) {
        for (int i = 0; i < n; i++) {
            if (array[i].equals(data)) {
                return true;
            }
        }
        return false;
    }

    // Return size of list.
    @Override
    public int size() {

        return n;
    }

    // Return "true" if list is empty.
    @Override
    public boolean isEmpty() {
        return n == 0;
    }

}