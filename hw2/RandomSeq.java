package hw2;

import java.util.ArrayList;
import java.util.Random;

class RandomSeq{

    private int n;
    private ArrayList<Integer> ranseq;

    public RandomSeq(int n){
        this.n = n;
    }

    public void create(int m){
        ranseq = new ArrayList<>();
        while (ranseq.size() < n){
            int i = new Random().nextInt(m+1);
            if (!ranseq.contains(i)){
                ranseq.add(i);
            }
        }
    }

    public void print(){
        for (Integer integer : ranseq) {
            System.out.print(integer + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int n =  10;
        int m = 15;
        RandomSeq rq = new RandomSeq(n);
        rq.create(m);
        rq.print();

    }
}