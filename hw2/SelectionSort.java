package hw2;

class SelectionSort<T extends Comparable<T>> {

    private T[] a;
    private int count_swap = 0;
    private int status = 0;
    private int compare_count = 0;

    public SelectionSort(T[] a) {
        this.a = a;
    }

    /**
     * @return the compare_count
     */
    public int getCompare_count() {
        return compare_count;
    }

    /**
     * @return the count_swap
     */
    public int getCount_swap() {
        return count_swap;
    }

    public T[] sort(){
        T [] arr = a.clone();
        for (int i = 0; i < arr.length-1; i++) {
            int minIndex = i;
            for (int j = i+1; j < arr.length; j++) {
                if (arr[j].compareTo(arr[minIndex])<0){
                    minIndex = j;
                }
                compare_count++;
            }
            T t = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = t;
            count_swap++;
            status++;
            // Print status of array.
            //System.out.print("Status "+ status + ": ");
            //output(arr);
        }


        return arr;
    }

    public void output(T[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }
}