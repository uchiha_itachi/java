package hw2;
class DeckTest{

    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.shuffle();
        System.out.println("Deck is shuffled: ");
        deck.showDeck();
        deck.sortDeck();
        System.out.println("Deck is sorted: ");
        deck.showDeck();
    }
}