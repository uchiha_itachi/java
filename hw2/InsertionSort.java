package hw2;
class InsertionSort<T extends Comparable<T>>{

    private T[] a;
    private int count_swap = 0;
    private int status = 0;
    private int compare_count = 0;

    public InsertionSort(T [] a){
        this.a = a;
    }

    /**
     * @return the compare_count
     */
    public int getCompare_count() {
        return compare_count;
    }

    /**
     * @return the count_swap
     */
    public int getCount_swap() {
        return count_swap;
    }

    public T[] sort(){
        T [] arr = a.clone();
        for (int i = 1; i < arr.length; i++) {
            T key = arr[i];
            int j = i - 1;
            while(j>=0 && arr[j].compareTo(key)>0){
                compare_count++;
                arr[j+1] = arr[j];
                j--;
                count_swap++;
            }
            arr[j+1] = key;
            count_swap++;
            status++;
            // Print status of Array.
            // System.out.print("Status " + status + " :");
            // output(arr);
        }
        return arr;
    }


    public void output(T[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }

}