package hw2;

import java.util.Random;
import java.util.Scanner;
class SortingTest {

    private static Integer [] a;

    public static void input(){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.close();
        a = new Integer[n];
        Random r = new Random();
        for (int i = 0; i < a.length; i++) {
            a[i] = r.nextInt(1000);
        }
    }

    public static void bubbleSortTest() {
        BubbleSort<Integer> bs = new BubbleSort<Integer>(a);
        long start = System.currentTimeMillis();
        Integer [] sorted =bs.sort();
        long stop = System.currentTimeMillis();
        System.out.print("Array after sorting: ");
        bs.output(sorted);
        System.out.println("Time: " + (stop - start) + " ms");
        System.out.println("Number of swap: " + bs.getCount_swap());
        System.out.println("Number of comparison: " + bs.getCompare_count());
    }
    public static void selectionSortTest() {
        SelectionSort<Integer> sls = new SelectionSort<Integer>(a);
        long start = System.currentTimeMillis();
        Integer [] sorted =sls.sort();
        long stop = System.currentTimeMillis();
        System.out.print("Array after sorting: ");
        sls.output(sorted);
        System.out.println("Time: " + (stop - start) + " ms");
        System.out.println("Number of swap: " + sls.getCount_swap());
        System.out.println("Number of comparison: " + sls.getCompare_count());
    }

    public static void insertionSortTest() {
        InsertionSort<Integer> iss = new InsertionSort<Integer>(a);
        long start = System.currentTimeMillis();
        Integer [] sorted =iss.sort();
        long stop = System.currentTimeMillis();
        System.out.print("Array after sorting: ");
        iss.output(sorted);
        System.out.println("Time: " + (stop - start) + " ms");
        System.out.println("Number of swap: " + iss.getCount_swap());
        System.out.println("Number of comparison: " + iss.getCompare_count());
    }




    public static void main(String[] args) {
        // Test with random array.
        input();
        System.out.println("Bubble Sort Test: ");
        bubbleSortTest();
        System.out.println();
        System.out.println("Selection Sort Test: ");
        selectionSortTest();
        System.out.println();
        System.out.println("Insertion Sort Test: ");
        insertionSortTest();

        // // Test with sorted array.
        // input();
        // a = new BubbleSort<Integer>(a).sort();
        // System.out.println("Bubble Sort Test: ");
        // bubbleSortTest();
        // System.out.println();
        // System.out.println("Selection Sort Test: ");
        // selectionSortTest();
        // System.out.println();
        // System.out.println("Insertion Sort Test: ");
        // insertionSortTest();
    }

}