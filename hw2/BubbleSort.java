package hw2;
class BubbleSort<T extends Comparable<T>> {

    private T [] a;
    private int count_swap = 0;
    private int status= 0;
    private int compare_count = 0;

    public BubbleSort(T [] a){
        this.a = a;
    }
    
    /**
     * @return the count_swap
     */
    public int getCount_swap() {
        return count_swap;
    }

    /**
     * @return the compare_count
     */
    public int getCompare_count() {
        return compare_count;
    }

    public T[] sort(){
        T [] arr = a.clone();
        for (int i = 0; i < a.length-1; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if(arr[i].compareTo(arr[j])>0){
                    T t = arr[i];
                    arr[i] = arr[j];
                    arr[j] = t;
                    count_swap++;
                }
                compare_count++;
                status++;
                // Print status of Array.
                // System.out.print("Status " + status + ": ");
                // output(arr);
            }
        }
        return arr;
    }

    public void output(T[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }

}