package hw2;

import java.util.Comparator;

class CompareCard implements Comparator<Card>{

    @Override
    public int compare(Card c1, Card c2) {
        // Compare rank.
        if(c1.getRank() > c2.getRank()) return 1;
        if(c1.getRank() < c2.getRank()) return -1;

        // if rank is equal. Compare suit.
        if(c1.getSuit().ordinal() > c2.getSuit().ordinal()) return 1;
        if(c1.getSuit().ordinal() < c2.getSuit().ordinal()) return -1;
        return 0;
    }
    
}