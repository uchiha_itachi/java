package hw2;

import java.util.Arrays;
import java.util.Random;

class Deck {
    private Card[] deck;

    // Create a deck with 52 cards.
    public Deck() {
        int j = 0;
        deck = new Card[52];
        for (int i = 1; i < 14; i++) {
            for (Suit s : Suit.values()) {
                Card c = new Card(i, s);
                deck[j] = c;
                j++;
            }
        }
    }

    /**
     * @return the deck
     */
    public Card[] getDeck() {
        return deck;
    }

    public void showDeck() {
        for (int i = 0; i < deck.length; i++) {
            System.out.println(deck[i].toString());
        }
    }

    public void shuffle() {
        Random r = new Random();
        for (int i = 0; i < deck.length; i++) {
            int j = r.nextInt(deck.length - i);
            Card t = deck[i];
            deck[i] = deck[j];
            deck[j] = t;
        }
    }

    public void sortDeck() {
        //Sorting by Arrays.sort.
        Arrays.sort(deck, new CompareCard());
        

        // // Sorting by sorting algorithms wrote.
        // BubbleSort<Card> bs = new BubbleSort<>(deck);
        // deck = bs.sort();
    }


}