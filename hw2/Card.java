package hw2;

class Card implements Comparable<Card> {
    private int rank;
    private Suit suit;

    public Card(int rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * @param suit the suit to set
     */
    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    /**
     * @return the rank
     */
    public int getRank() {
        return rank;
    }

    /**
     * @return the suit
     */
    public Suit getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return rank + " " + suit;
    }

    @Override
    public int compareTo(Card other) {
        // Compare rank.
        if (this.getRank() > other.getRank())
            return 1;
        if (this.getRank() < other.getRank())
            return -1;

        // if rank is equal. Compare suit.
        if (this.getSuit().ordinal() > other.getSuit().ordinal())
            return 1;
        if (this.getSuit().ordinal() < other.getSuit().ordinal())
            return -1;
        return 0;
    }

}