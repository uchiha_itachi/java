package hw4_18001087.applications;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hw4_18001087.api.LinkedListQueue;
import hw4_18001087.api.LinkedListStack;

public class Palindrome {

    // Input String.
    public static String input(){
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        sc.close();
        return s;
    }

    // Format String.
    public static String format(String s){
        String str = "";
        Pattern p = Pattern.compile("\\w+");
        Matcher matcher = p.matcher(s);
        while(matcher.find()){
            str+= matcher.group();
        }
        return str.toLowerCase();
    }

    // Check palindrome.
    public static boolean check(String s){
        LinkedListStack<Character> st = new LinkedListStack<>();
        LinkedListQueue<Character> q = new LinkedListQueue<>();
        for (int i = 0; i < s.length(); i++) {
            st.push(s.charAt(i));
            q.enqueue(s.charAt(i));
        }
        for (int i = 0; i < s.length(); i++) {
            if(st.pop() != q.dequeue()){
                return false;
            }
            
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.print("Input String: ");
        String s = input();
        String str = format(s);
        System.out.println("Is palindrome: " + check(str));
    }
}