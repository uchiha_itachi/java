package hw4_18001087.applications;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hw4_18001087.api.LinkedListStack;

public class BranketCheck {
    // Input String.
    public static String input() {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        sc.close();
        return s;
    }

    // Format String.
    public static ArrayList<String> format(String s) {
        ArrayList<String> str = new ArrayList<>();
        Pattern p = Pattern.compile("\\d+|\\(|\\)|\\+|\\-|\\*|\\/|\\%");
        Matcher m = p.matcher(s);
        while (m.find()) {
            str.add(m.group());
        }
        return str;
    }

    // Check branket.
    public static boolean check(ArrayList<String> s) {
        LinkedListStack<String> branketSt = new LinkedListStack<>();
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).equals("(")) {
                branketSt.push(s.get(i));
            }
            if (s.get(i).equals(")")) {
                if (branketSt.isEmpty()) {
                    return false;
                }
                branketSt.pop();
            }
        }
        if (branketSt.isEmpty()) {
            return true;
        }
        return false;
    }

    // Return "true" if variable is operator.
    public static boolean isOperator(String ch) {
        if (ch.equals("+") || ch.equals("-") || ch.equals("*") || ch.equals("/") || ch.equals("%") || ch.equals("(")
                || ch.equals(")")) {
            return true;
        }
        return false;
    }

    public static int piority(String op) {
        if (op.equals("*") || op.equals("/") || op.equals("%")) {
            return 2;
        }
        if (op.equals("+") || op.equals("-")) {
            return 1;
        }
        return 0;
    }

    // Process operation.
    public static double processOperator(double num1, double num2, String op) throws ArithmeticException{
        switch (op) {
            case "+":
                return (num1 + num2);

            case "-":
                return (num1 - num2);

            case "*":
                return (num1 * num2);

            case "/":
                if(num2 == 0){
                    throw new ArithmeticException("Divide by zero");
                }
                return (num1 / num2);

            case "%":
                return (num1 % num2);

            default:
                return 0;
        }
    }

    // Calculate expression.
    public static Double calculate(ArrayList<String> s) {
        // check legal expression.
        if (!check(s)) {
            System.out.println("Illegal Expression");
            return null;
        }
        LinkedListStack<Double> st = new LinkedListStack<>(); // Operand stack.
        LinkedListStack<String> op = new LinkedListStack<>(); // Operator stack.
        for (int i = 0; i < s.size(); i++) {
            String ch = s.get(i);
            if (!isOperator(ch)) { // if element is a operand.
                st.push(Double.parseDouble(ch)); // Push to "st"
            } else if (ch.equals("(")) { // if element is "(".
                op.push(ch); // Push to "op"
            } else if (ch.equals(")")) { // if element is ")"
                // Get 1 operator and 2 operand to calculate.
                // Push result into "st".
                while (!op.isEmpty() && !op.top().equals("(")) {
                    double num2 = st.pop();
                    double num1 = st.pop();
                    String ops = op.pop();
                    st.push(processOperator(num1, num2, ops));
                }
                op.pop(); // Remove "(".
            } else { // if element is a operator.
                // while top of "op" has same or greater piority of current operator.
                // Get top operator and 2 operand to calculate.
                // Push result into "st".
                while (!op.isEmpty() && piority(ch) <= piority(op.top())) {
                    double num2 = st.pop();
                    double num1 = st.pop();
                    String ops = op.pop();
                    st.push(processOperator(num1, num2, ops));
                }
                op.push(ch); // Push current operator into "op".
            }
        }
        // calculate remaining operator.
        while (!op.isEmpty()) {
            double num2 = st.pop();
            double num1 = st.pop();

            String ops = op.pop();
            st.push(processOperator(num1, num2, ops));
        }
        return st.top();
    }

    public static void main(String[] args) {
        System.out.print("Enter your expression: ");
        String s = input();
        //String s = "((1+2) / 3 * 4)";
        ArrayList<String> str = format(s);
        System.out.println("Result: " + calculate(str));

    }
}