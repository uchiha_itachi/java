package hw4_18001087.applications.maze;

import javax.swing.JPanel;
import java.awt.*;

public class MazePanel extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private JPanel[][] maze;
    private int width = 580/11;
    private int height = 575/11;

    

    public MazePanel(int matrix[][]) {
        maze = new JPanel[matrix.length][matrix.length];
        this.setLayout(new GridLayout(maze.length, maze.length));
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                JPanel p = new JPanel();
                maze[i][j] = p;
                this.add(p);
                if (matrix[i][j] == -1) {

                    p.setBackground(Color.BLACK);

                }
                if (matrix[i][j] == 3) {
                    maze[i][j].setBackground(Color.GREEN);
                }
            }
        }
        // for (int i = 0; i < matrix.length; i++) {
        // for (int j = 0; j < matrix.length; j++) {
        // System.out.print(matrix[i][j] + " ");
        // }
        // System.out.println();
        // }
    }

    public void reload(int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                maze[i][j].setBackground(Color.WHITE);
                if (matrix[i][j] == -1) {

                    maze[i][j].setBackground(Color.BLACK);
                }
                if (matrix[i][j] == 3) {

                    maze[i][j].setBackground(Color.GREEN);
                }

            }
        }
    }

    

    public void draw(int[][] matrix, Graphics g) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j] == 3) {
                    g.setColor(Color.GREEN);
                    g.fillRect(j*width, i*height, width, height);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // maze[i][j].setBackground(Color.GREEN);
                }
            }
        }
    }

    public void move(int i, int j, int direct) {
       // System.out.println(i + " " + j);
        switch (direct) {
            case 0:
            // System.out.println(0);
                maze[i][j].setBackground(Color.WHITE);
                maze[i - 1][j].setBackground(Color.GREEN);
                break;
            case 1:
               // System.out.println(1);
                maze[i][j].setBackground(Color.WHITE);
                maze[i][j + 1].setBackground(Color.GREEN);
                break;
            case 2:
                //System.out.println(2);
                maze[i][j].setBackground(Color.WHITE);
                maze[i + 1][j].setBackground(Color.GREEN);
                break;
            case 3:
                //System.out.println(3);
                maze[i][j].setBackground(Color.WHITE);
                maze[i][j - 1].setBackground(Color.GREEN);
                break;
            default:
                break;
        }
    }
    // public static void main(String[] args) {
    // MazePanel m = new MazePanel();
    // }

}