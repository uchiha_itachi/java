package hw4_18001087.applications.maze;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class App extends JFrame {

    /**
     *
     */
    private int status = 0;
    private Algorithm a = new Algorithm();
    private int[][] matrix = a.getMatrix();
    private Integer[] currentPos = { 0, 1 };
    private static final long serialVersionUID = 1L;
    private MazePanel mainPanel;
    private JPanel mazePanel;
    private JButton solveButton;
    private JButton newGameButton;
    private JButton changeMaze;
    private JButton tutorial;

    public App() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(700, 700);
        this.setLayout(null);
        this.setResizable(false);
        this.setLocationRelativeTo(null);;

        JPanel buttonPanel = new JPanel();
        buttonPanel.setBounds(0, 0, 700, 50);
        buttonPanel.setLayout(new GridLayout(0, 5));

        JButton playButton = new JButton("Play");
        playButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                solveButton.setEnabled(true);
                changeMaze.setEnabled(false);
                status = 1;
                System.out.println("Play");
            }

        });
        solveButton = new JButton("Solve");
        solveButton.setEnabled(false);
        solveButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                a.find();
                a.drawLine();
                mainPanel.reload(a.getMatrix());
                // mainPanel.draw(a.getMatrix(), mainPanel.getGraphics());
                System.out.println("Clicked Solve");

            }

        });
        changeMaze = new JButton("ChangeMaze");
        changeMaze.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                a.reset();
                a.createMatrix();
                a.createMaze();
                a.format();
                currentPos[0] = 0;
                currentPos[1] = 1;
                mainPanel.reload(a.getMatrix());
                System.out.println("Change Maze");

            }

        });
        newGameButton = new JButton("New Game");
        newGameButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                changeMaze.setEnabled(true);
                solveButton.setEnabled(false);
                a.reset();
                a.createMatrix();
                a.createMaze();
                a.format();
                currentPos[0] = 0;
                currentPos[1] = 1;
                status = 0;
                mainPanel.reload(a.getMatrix());
                System.out.println("New Game");
            }

        });
        tutorial = new JButton("Tutorial");
        tutorial.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                JFrame t = new TutorialFrame();
                t.setVisible(true);

            }

        });


        buttonPanel.add(playButton);
        buttonPanel.add(solveButton);
        buttonPanel.add(changeMaze);
        buttonPanel.add(newGameButton);
        buttonPanel.add(tutorial);
        
        // Maze panel
        mazePanel = new JPanel();
        mazePanel.setLayout(new BorderLayout());
        mazePanel.setBounds(0, 50, 700, 620);

        a.createMatrix();
        a.createMaze();
        a.format();
        mainPanel = new MazePanel(a.getMatrix());
       
        mazePanel.add(mainPanel, BorderLayout.CENTER);

        this.add(mazePanel);
        this.add(buttonPanel);

        playButton.addKeyListener(new CustomKeyListener());
        
    }

    
    class CustomKeyListener implements KeyListener {

        @Override
        public void keyPressed(KeyEvent e) {
            // Up
            if (status == 1 && e.getKeyCode() == KeyEvent.VK_W) {
                int i = currentPos[0];
                int j = currentPos[1];
                if (i - 1 > 0 && matrix[i - 1][j] == 0) {
                    mainPanel.move(i, j, 0);
                    currentPos[0] = i - 1;
                    System.out.println("Up");
                }

            }
            // down
            if (status == 1 && e.getKeyCode() == KeyEvent.VK_S) {
                int i = currentPos[0];
                int j = currentPos[1];
                if (matrix[i + 1][j] == 0) {
                    mainPanel.move(i, j, 2);
                    currentPos[0] = i + 1;
                    System.out.println("Down");
                }

            }
            // left
            if (status == 1 && e.getKeyCode() == KeyEvent.VK_A) {
                int i = currentPos[0];
                int j = currentPos[1];
                if (matrix[i][j - 1] == 0) {
                    mainPanel.move(i, j, 3);
                    currentPos[1] = j - 1;
                    System.out.println("Left");
                }

            }
            // right.
            if (status == 1 && e.getKeyCode() == KeyEvent.VK_D) {
                int i = currentPos[0];
                int j = currentPos[1];
                if (currentPos[0] == matrix.length - 2 && currentPos[1] == matrix.length - 1) {
                    JOptionPane.showMessageDialog(mainPanel, "Congratulation! \n You have completed the maze");
                    solveButton.setEnabled(false);
                    changeMaze.setEnabled(true);
                    a.reset();
                    a.createMatrix();
                    a.createMaze();
                    a.format();
                    status = 0;
                    currentPos[0] = 0;
                    currentPos[1] = 1;
                    mainPanel.reload(a.getMatrix());
                } else if (matrix[i][j + 1] == 0) {
                    mainPanel.move(i, j, 1);
                    currentPos[1] = j + 1;
                    System.out.println("Right");
                }

            }

        }

        @Override
        public void keyReleased(KeyEvent arg0) {

        }

        @Override
        public void keyTyped(KeyEvent arg0) {

        }
    }
}