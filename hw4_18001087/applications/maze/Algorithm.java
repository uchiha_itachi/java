package hw4_18001087.applications.maze;

import java.util.ArrayList;
import java.util.Random;

import hw4_18001087.api.LinkedListStack;

public class Algorithm {

    private int matrixSize = 41;
    private int[][] matrix = new int[formatMatrixSize()][formatMatrixSize()];
    private int count = 0;
    private Integer[] pos = { 0, 1 };
    private LinkedListStack<Integer[]> st = new LinkedListStack<>();

    // Generate matrix
    public void createMatrix() {
        
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i % 2 != 0 && j % 2 != 0) {
                    matrix[i][j] = 0;
                    count++;
                } else {
                    matrix[i][j] = -1;
                }
            }
        }
    }

    // Generate maze.
    public void createMaze() {
        Random r = new Random();
        pos[0] = 1;
        pos[1] = 1;
        matrix[1][1] = 2;
        count--;
        while (count > 0) {
            int i = pos[0], j = pos[1];
            ArrayList<Integer> direct = direct(i, j, "create");
            int d = r.nextInt(direct.size());
            int k = direct.get(d);
            openWay(i, j, k);
        }

    }

    public void find() {
        Random r = new Random();
        // print();
        while ((pos[0] != matrix.length - 2) || (pos[1] != matrix.length - 1)) {
            int i = pos[0];
            int j = pos[1];
            ArrayList<Integer> d = direct(i, j, "find");
            Integer[] curPos = { i, j };
            if (d.size() == 1) {
                st.push(curPos);
                go(i, j, d.get(0));
            } else if (d.size() == 0) {
                pos = st.pop();
            } else {
                st.push(curPos);
                int k = r.nextInt(d.size());
                go(i, j, d.get(k));
            }
            // printSt();
            // System.out.println("POS: "+ pos[0] + " " + pos[1]);

        }
        // System.out.println("Stop at: " + pos[0] + " " + pos[1]);
    }

    public void reset() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = 0;

            }
        }
        pos[0] = 0;
        pos[1] = 1;
        st = new LinkedListStack<>();
        count = 0;
    }

    public void drawLine() {
        while (!st.isEmpty()) {
            Integer[] p = st.pop();
            matrix[p[0]][p[1]] = 3;
        }
        matrix[matrix.length - 2][matrix.length - 1] = 3;
    }

    // private void printSt() {
    // Iterator<Integer[]> it = st.iterator();
    // while (it.hasNext()) {
    // Integer c[] = it.next();
    // System.out.print(c[0] + " " + c[1] + " |");
    // }
    // System.out.println();
    // }

    private void go(int i, int j, int direct) {
        switch (direct) {
            case 0:
                pos[0] = i - 1;
                matrix[pos[0]][pos[1]] = 2;
                break;
            case 1:
                pos[1] = j + 1;
                matrix[pos[0]][pos[1]] = 2;
                break;
            case 2:
                pos[0] = i + 1;
                matrix[pos[0]][pos[1]] = 2;
                break;
            case 3:
                pos[1] = j - 1;
                matrix[pos[0]][pos[1]] = 2;
                break;
            default:
                break;
        }
    }

    private void openWay(int i, int j, int k) {
        switch (k) {
            case 0:
                if (matrix[i - 2][j] == 0) {
                    count--;
                    matrix[i - 1][j] = 2;
                }
                matrix[i - 2][j] = 2;
                i = i - 2;
                pos[0] = i;
                break;
            case 1:
                if (matrix[i][j + 2] == 0) {
                    count--;
                    matrix[i][j + 1] = 2;
                }
                matrix[i][j + 2] = 2;
                j = j + 2;
                pos[1] = j;
                break;
            case 2:
                if (matrix[i + 2][j] == 0) {
                    count--;
                    matrix[i + 1][j] = 2;
                }
                matrix[i + 2][j] = 2;
                i = i + 2;
                pos[0] = i;
                break;
            case 3:
                if (matrix[i][j - 2] == 0) {
                    count--;
                    matrix[i][j - 1] = 2;
                }
                matrix[i][j - 2] = 2;
                j = j - 2;
                pos[1] = j;
                break;
            default:
                break;
        }
    }

    public ArrayList<Integer> direct(int i, int j, String option) {
        ArrayList<Integer> direc = new ArrayList<>();
        if (option.equals("create")) {
            if ((i - 2) > 0) {
                direc.add(0);
            }
            if ((i + 2) < matrix.length) {
                direc.add(2);
            }
            if ((j + 2) < matrix.length) {
                direc.add(1);
            }
            if ((j - 2) > 0) {
                direc.add(3);
            }
        }
        if (option.equals("find")) {
            if (i - 1 > 0 && matrix[i - 1][j] == 0) {
                direc.add(0);
            }
            if (matrix[i][j + 1] == 0) {
                direc.add(1);
            }
            if (matrix[i + 1][j] == 0) {
                direc.add(2);
            }
            if (matrix[i][j - 1] == 0) {
                direc.add(3);
            }
        }
        return direc;

    }

    public void print() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void format() {
        for (int i = 1; i < matrix.length; i++) {
            for (int j = 1; j < matrix.length; j++) {
                if (matrix[i][j] == 2) {
                    matrix[i][j] = 0;
                }
            }
        }
        matrix[0][1] = 3;
        // matrix[1][1] = 2;
        matrix[matrix.length - 2][matrix.length - 1] = 0;
        pos[0] = 0;
        pos[1] = 1;
    }

    /**
     * @return the matrix
     */
    public int[][] getMatrix() {
        return matrix;
    }
    private int formatMatrixSize(){
        if(matrixSize % 2 == 0){
            return matrixSize+1;
        }
        return matrixSize;
    }

    public static void main(String[] args) {
        // Algorithm a = new Algorithm();
        // a.createMatrix();
        // a.createMaze();
        // a.setMatrix();
        // a.find();
        // a.drawLine();
        // a.print();
    }

}