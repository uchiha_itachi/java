package hw4_18001087.applications.maze;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import java.awt.*;

public class TutorialFrame extends JFrame{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public TutorialFrame(){
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setSize(400, 200);
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());
        this.setResizable(false);

        String text = "1. Click \"Play\" to start a game. \n" + 
                      "2. Using W, A, S, D to move Up, Left, Down, Right. \n" + 
                      "3. Click \"Solve\" to find the way out.\n" + 
                      "4. Click \" Change Maze\" to create a new maze.\n" +
                      "5. Click \"New Game\" to create a new game.\n" + 
                      "6. If you want to change the size of maze,\n "+
                        " please go to file\"Algorithm.java\" and change the \n" +
                        "\" matrixSize\" variable. I really apologize for inconvenience";
        JTextArea tutorialText = new JTextArea(text);
        tutorialText.setEditable(false);
        this.add(tutorialText);

    }
}