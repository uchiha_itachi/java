package hw4_18001087.test;

import hw4_18001087.api.ArrayQueue;
import hw4_18001087.api.LinkedListQueue;

public class QueueTest {
    public static void ArrayQueueTest(){
        ArrayQueue<Integer> aq = new ArrayQueue<>(10);
        aq.enqueue(1);
        aq.enqueue(2);
        aq.enqueue(3);
        aq.enqueue(3);
        aq.enqueue(3);
        aq.enqueue(3);
        aq.enqueue(3);
        aq.enqueue(3);
        aq.enqueue(3);
        aq.enqueue(3);
        System.out.println("Top: " + aq.peek());
        aq.print();
    }

    public static void LinkedListTest(){
        LinkedListQueue<Integer> lq = new LinkedListQueue<>();
        lq.enqueue(1);
        lq.enqueue(2);
        lq.print();
        System.out.println("Top: " + lq.dequeue());
        lq.enqueue(10);
        lq.print();
        System.out.println("Top: " + lq.dequeue());
        lq.print();
    }
    public static void main(String[] args) {
       LinkedListTest();
    }
}