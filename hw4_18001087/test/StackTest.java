package hw4_18001087.test;

import java.util.Iterator;

import hw4_18001087.api.*;

public class StackTest {

    public static void ArrayListStackTest() {
        ArrayListStack<Integer> st = new ArrayListStack<>();
        // System.out.println(st.pop());
        st.push(1);
        st.push(2);
        st.push(3);
        System.out.println(st.pop());
        st.push(1);
        System.out.println("Top element: " + st.top());
        Iterator<Integer> it = st.iterator();
        System.out.print("All stack's element: ");
        while (it.hasNext()) {
            System.out.print(it.next() + " ");
        }
        System.out.println();
    }

    public static void LinkedListStackTest() {
        LinkedListStack<Integer [] > st = new LinkedListStack<>();
        Integer [] a = {1, 2};
        Integer [] b = {4, 3};
        st.push(a);
        st.push(b);

        System.out.println("Top element: " + st.top());
        System.out.print("All stack's element: ");
        Iterator<Integer[]> it = st.iterator();
        while (it.hasNext()) {
            Integer c[] = it.next();
            System.out.print(c[0] + " " + c[1]);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        // LinkedListStackTest();
        int i = 1;
        int j = 2;
        while(i != 10 && j!= 10 ){
            System.out.println("YEs: " + i);
            i++;
            j++;
        }
        System.out.println("stop at: " + i + " " + j);
    }
}