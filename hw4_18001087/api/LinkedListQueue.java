package hw4_18001087.api;

import java.util.Iterator;

public class LinkedListQueue<E> implements QueueInterface<E> {
    class Node {
        E element;
        Node next;
    }

    private Node top = null; // Element at top of queue

    @Override
    public Iterator<E> iterator() {
        Iterator<E> it = new Iterator<E>() {
            Node t = top;
            @Override
            public boolean hasNext() {
                return t!= null;
            }

            @Override
            public E next() {
                E elem = t.element;
                t = t.next;
                return elem;
            }
        };
        return it;
    }

    public void print(){
        Iterator<E> it = iterator();
        System.out.print("Queue: ");
        while(it.hasNext()){
            System.out.print(it.next() + " ");
        }
        System.out.println();
    }
    @Override
    public void enqueue(E element) {
        if(top == null){
            top = new Node();
            top.element = element;
            top.next = null;
        }
        else {
            Node t = top;
            while (t.next!= null){
                t = t.next;
            }   
            Node n = new Node();
            n.element = element;
            n.next = null;
            t.next = n;
        }
    }

    public E peek() throws NullPointerException{
        if (top == null){
            throw new NullPointerException("Queue is empty");
        }
        return top.element;
    }
    @Override
    public E dequeue() throws NullPointerException {
        if (top == null){
            throw new NullPointerException("Queue is empty");
        }
        E elem = top.element;
        top = top.next;
        return elem;

    }

    @Override
    public boolean isEmpty() {
        return top == null;
    }

}