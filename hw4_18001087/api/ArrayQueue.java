package hw4_18001087.api;

import java.util.Iterator;

public class ArrayQueue<E> implements QueueInterface<E> {

    private E[] queue;
    private int n = 0; 
    private int top = 0; // Index of top element
    private int count = 0; // Number of element in queue.
    private int default_size = 100;

   // Generate queue with given capacity.
    @SuppressWarnings("unchecked")
    public ArrayQueue(int capacity) {
        n = capacity;
        queue = (E[]) new Object[capacity];
    }
    
    // Generate queue with default size.
    @SuppressWarnings("unchecked")
    public ArrayQueue() {
        n = default_size;
        queue = (E[]) new Object[default_size];
    }

    // Add an element to queue.
    @Override
    public void enqueue(E element) throws IllegalStateException {
        if (count == n) {
            throw new IllegalStateException("Queue is full");

        }
        int index = (top + count) % n;
        queue[index] = element;
        count++;
    }

    // Remove top element of queue.
    @Override
    public E dequeue() throws NullPointerException {
        if (isEmpty()) {
            throw new NullPointerException("Queue is empty");
        }
        E elem = queue[top];
        queue[top] = null;
        top = (top + 1) % n;
        count--;
        return elem;
    }
    public E peek() throws NullPointerException{
        if (isEmpty()) {
            throw new NullPointerException("Queue is empty");
        }
        return queue[top];
    }

    // Return "true" if queue is empty.
    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    // Return an iterator.
    @Override
    public Iterator<E> iterator() {
        return new ArrayQueueIterator();
    }

    public void print(){
        Iterator<E> it = iterator();
        System.out.print("Queue: ");
        while(it.hasNext()){
            System.out.print(it.next() + " ");
        }
        System.out.println();
        
    }

    class ArrayQueueIterator implements Iterator<E> {
        private int current = top;
        private int num = 0;

        @Override
        public boolean hasNext() {
            return num < count;
        }

        @Override
        public E next() {
            E data = queue[(current + num) % n];
            num++;
            return data;
        }
    }
}
