import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Test extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JPanel panel;

    public Test() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(700, 700);

        this.setLayout(new BorderLayout());

        JButton a = new JButton("Click");
        a.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_W) {
                    panel.setBackground(Color.BLACK);
                }

            }

            @Override
            public void keyReleased(KeyEvent arg0) {

            }

            @Override
            public void keyTyped(KeyEvent arg0) {

            }

        });
        a.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                panel.setBackground(Color.BLACK);

            }

        });
        JButton b = new JButton("B");
        b.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_A) {
                    panel.setBackground(Color.BLUE);
                }

            }

            @Override
            public void keyReleased(KeyEvent arg0) {

            }

            @Override
            public void keyTyped(KeyEvent arg0) {

            }

        });
        JButton c = new JButton("B");
        JButton d = new JButton("B");

        panel = new JPanel();
        panel.setLayout(new GridLayout(0, 4));

        this.add(a, BorderLayout.NORTH);
        this.add(panel, BorderLayout.CENTER);
        this.add(b, BorderLayout.WEST);
        this.add(c, BorderLayout.SOUTH);
        this.add(d, BorderLayout.EAST);

    }

    public static void main(String[] args) {
        Test t = new Test();
        t.setVisible(true);
    }
}