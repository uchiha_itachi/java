package hw5_18001087.api;

public class LinkedBinaryTree<E, T> implements BinaryTreeInterface<T> {

    private Node<E> root = null;
    private int size = 0;

    public LinkedBinaryTree(Node<E> root) {
        this.root = root;
    }

    public LinkedBinaryTree() {

    }

    @Override
    @SuppressWarnings("unchecked")
    public T root() {
        if (root == null) {
            System.out.println("Tree is empty...-_-");
        }
        return (T) root;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public int numChildren(T p) {
        Node<E> n = (Node<E>) p;
        if (n.getLeft() != null && n.getRight() != null) {
            return 2;
        } else if (n.getLeft() != null && n.getRight() != null) {
            return 1;
        }
        return 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T parent(T p) {
        Node<E> n = (Node<E>) p;
        if (n.getParent() == null) {
            System.out.println("Parent not found");
        }
        return (T) n.getParent();
    }

    @Override
    @SuppressWarnings("unchecked")
    public T left(T p) {
        Node<E> n = (Node<E>) p;
        if (n.getLeft() == null) {
            System.out.println("Left child not found");
        }
        return (T) n.getLeft();
    }

    @Override
    @SuppressWarnings("unchecked")
    public T right(T p) {
        Node<E> n = (Node<E>) p;
        if (n.getRight() == null) {
            System.out.println("Right child not found");
        }
        return (T) n.getRight();
    }

    @Override
    @SuppressWarnings("unchecked")
    public T sibling(T p) {
        Node<E> n = (Node<E>) p;
        Node<E> parent = n.getParent();
        if (parent.getLeft() == n) {
            return (T) parent.getRight();
        }
        if (parent.getRight() == n) {
            return (T) parent.getLeft();
        }
        System.out.println("No sibling found");
        return null;
    }

    @SuppressWarnings("unchecked")
    public E getData(T p) {
        if (p == null) {
            System.out.println("This node has no value");
        }
        Node<E> n = (Node<E>) p;
        return n.getElement();
    }

    public Node<E> addRoot(E element) {
        if (root == null) {
            root = new Node<E>(element, null, null, null);
            size++;
            return root;
        }
        return null;
    }

    public Node<E> addLeft(Node<E> p, E element) {
        if (p.getLeft() == null) {
            p.setLeft(new Node<E>(element, p, null, null));
            size++;

        } else {
            p.getLeft().setElement(element);
        }
        return p.getLeft();
    }

    public Node<E> addRight(Node<E> p, E element) {
        if (p.getRight() == null) {
            p.setRight(new Node<E>(element, p, null, null));
            size++;
        } else {
            p.getRight().setElement(element);
        }
        return p.getRight();
    }

    public void set(Node<E> p, E element) {
        p.setElement(element);
    }

    public void print(Node<E> root, int lvl) {
        if (root == null) {
            return;
        }
        print(root.getRight(), lvl + 1);
        for (int i = 0; i < lvl; i++) {
            System.out.print("\t");
        }
        System.out.println(root.getElement());
        print(root.getLeft(), lvl + 1);
    }

}