package hw5_18001087.api;

public class ArrayBinaryTree<E, T> implements BinaryTreeInterface<T> {

    private E[] array;
    private int size = 0;
    private int defaultSize = 100;

    @SuppressWarnings("unchecked")
    public ArrayBinaryTree() {
        this.array = (E[]) new Object[defaultSize + 1];
    }

    @SuppressWarnings("unchecked")
    public ArrayBinaryTree(int capacity) {
        this.array = (E[]) new Object[capacity + 1];
    }

    @Override
    @SuppressWarnings("unchecked")
    public T root() {
        if (array[1] != null) {
            return (T) Integer.valueOf(1);
        }
        System.out.println("Tree is empty...-_-");
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int numChildren(T p) {
        if (p == null) {
            System.out.println("Null...-_-");
            return -1;
        }
        int i = (int) p;
        if (array[i] == null) {
            return -1;
        }
        int count = 0;
        if (left(p) != null) {
            count++;
        }
        if (right(p) != null) {
            count++;
        }
        return count;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T parent(T p) {
        if (p == null) {
            System.out.println("Null...-_-");
            return null;
        }
        int i = (int) p;
        if (i / 2 < 1 || i >= array.length || array[i] == null) {
            System.out.println("Index " + i + " has no parent...-_-");
            return null;
        }
        T t = (T) Integer.valueOf(i / 2);
        return t;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T left(T p) {
        if (p == null) {
            System.out.println("Null...-_-");
            return null;
        }
        int i = (int) p;
        if (i < 1 || i >= (array.length + 1) / 2 || array[2 * i] == null) {
            System.out.println("Parent " + i + " has no left child...-_-");
            return null;
        }
        T t = (T) Integer.valueOf((2 * i));
        return t;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T right(T p) {
        if (p == null) {
            System.out.println("Null...-_-");
            return null;
        }
        int i = (int) p;
        if (i < 1 || i >= (array.length) / 2 || array[(2 * i) + 1] == null) {
            System.out.println("Parent " + i + " has no right child...-_-");
            return null;
        }
        T t = (T) Integer.valueOf((2 * i) + 1);
        return t;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T sibling(T p) {
        if (p == null) {
            System.out.println("Null...-_-");
            return null;
        }
        int i = (int) p;
        if (i < 1 || i >= size()) {
            System.out.println("Element " + i + " has no sibling...-_-");
            return null;
        }

        if (i % 2 == 0 && array[i + 1] != null) { // if i is left child
            return (T) Integer.valueOf(i + 1);
        }
        if (i % 2 == 1 && array[i - 1] != null) {// if i is right child
            return (T) Integer.valueOf(i - 1);
        }
        System.out.println("Element " + i + " has no sibling...-_-");
        return null;
    }

    public void setRoot(E element) {
        if (array[1] == null) {
            size++;
        }
        array[1] = element;
    }

    public void setLeft(int p, E element) {
        if (p < 1 || p >= array.length || array[p] == null) {
            System.out.println("Parent " + p + " not found...-_-");
            return;
        }
        if ((2 * p) >= array.length) {
            System.out.println("Left child of " + p + " exceed size of tree...-_-");
            return;
        }
        if (array[2 * p] == null) {
            size++;
        }
        array[2 * p] = element;
    }

    public void setRight(int p, E element) {
        if (p < 1 || p >= array.length || array[p] == null) {
            System.out.println("Parent " + p + " not found...-_-");
            return;
        }
        if ((2 * p) + 1 >= array.length) {
            System.out.println("Right child of " + p + " exceed size of tree...-_-");
            return;
        }
        if (array[(2 * p) + 1] == null) {
            size++;
        }
        array[(2 * p) + 1] = element;
    }

    public E getElement(int p) {
        if (p < 1 || p >= array.length) {
            System.out.println("Index " + p + " is out of tree...-_-");
            return null;
        }
        return array[p];
    }

    public void inorderPrint(int p) {
        if (p < array.length && array[p] != null) {
            System.out.print(array[p] + " ");
            inorderPrint(2 * p);
            inorderPrint(2 * p + 1);
        }
    }

    public void print(Integer p, int lvl) {
        if (array[p] == null) {
            return;
        } else {
            print(2 * p + 1, lvl + 1);
            for (int i = 0; i < lvl; i++) {
                System.out.print("\t");
            }
            System.out.println(array[p]);
            print(2 * p, lvl + 1);
        }
    }

}