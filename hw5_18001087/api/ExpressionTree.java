package hw5_18001087.api;

public class ExpressionTree<E> extends LinkedBinaryTree<E, Node<E>> {

    public ExpressionTree(Node<E> root) {
        super(root);
    }

    public void preorderPrint(Node<E> p) {
        if (p != null) {
            System.out.print(p.getElement() + " ");
            preorderPrint(p.getLeft());
            preorderPrint(p.getRight());
        }
    }

    public void inorderPrint(Node<E> p) {
       if(super.numChildren(p) == 0){ // if p is leaf.
           System.out.print(p.getElement());
       } else {
           System.out.print("(");
           inorderPrint(p.getLeft());
           System.out.print(" " + p.getElement() + " ");
           inorderPrint(p.getRight());
           System.out.print(")");
       }
    }

    public void postorderPrint(Node<E> p) {
        if (p != null) {
            postorderPrint(p.getLeft());
            postorderPrint(p.getRight());
            System.out.print(p.getElement() + " ");
        }
    }

}