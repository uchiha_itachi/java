package hw5_18001087.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import hw5_18001087.api.ArrayBinaryTree;
import hw5_18001087.api.LinkedBinaryTree;
import hw5_18001087.api.Node;


public class TreeTest {
    public static void input(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static void ArrayTreeTest() {
        ArrayBinaryTree<Integer, Integer> a = new ArrayBinaryTree<>(4);
        a.setRoot(1);
        a.setLeft(1, 2);
        a.setRight(1, 3);
        a.setLeft(2, 4);



        System.out.println("Root: " + a.root());
        System.out.println("Is empty: " + a.isEmpty());
        System.out.println("Size: " + a.size());
        System.out.print("Tree: ");
        a.inorderPrint(1);
        System.out.println();
    }
    public static void LinkedTreeTest(){
        LinkedBinaryTree<Integer, Node<Integer>> lt = new LinkedBinaryTree<>();
        lt.addRoot(1);
        lt.addLeft(lt.root(), 2);
        lt.addLeft(lt.root().getLeft(), 4);
        lt.addRight(lt.root().getLeft(), 5);
        lt.addRight(lt.root(), 3);
        lt.addLeft(lt.root().getRight(), 6);
        lt.addRight(lt.root().getRight(),7);
        lt.print(lt.root(), 1);
        System.out.println();
    }

    public static void main(String[] args) {
        //ArrayTreeTest();
        //LinkedTreeTest();
    }
}