package hw5_18001087.app;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hw5_18001087.api.ExpressionTree;
import hw5_18001087.api.Node;

public class CalculateExpression {

    public String input() {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        sc.close();
        return s;
    }

    public ArrayList<String> format(String s) {
        ArrayList<String> exp = new ArrayList<>();
        String regex = "\\d+|\\(|\\)|\\+|\\-|\\*|\\/|\\%";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);
        s = s.replaceAll("\\s+", "");
        String str = "";
        while (matcher.find()) {
            exp.add(matcher.group());
            str += matcher.group();
        }
        if (!str.equals(s)) {
            return null;
        }
        // check branket.
        Stack<String> branketSt = new Stack<>();
        for (int i = 0; i < exp.size(); i++) {
            if (exp.get(i).equals("(")) {
                branketSt.push(exp.get(i));
            }
            if (exp.get(i).equals(")")) {
                if (branketSt.isEmpty()) {
                    return null;
                }
                branketSt.pop();
            }
        }
        if (branketSt.isEmpty()) {
            return exp;
        }
        return null;
    }

    public boolean checkBracket(ArrayList<String> exp) {
        Stack<String> branketSt = new Stack<>();
        for (int i = 0; i < exp.size(); i++) {
            if (exp.get(i).equals("(")) {
                branketSt.push(exp.get(i));
            }
            if (exp.get(i).equals(")")) {
                if (branketSt.isEmpty()) {
                    return false;
                }
                branketSt.pop();
            }
        }
        if (branketSt.isEmpty()) {
            return true;
        }
        return false;

    }

    public ArrayList<String> toPostfix(ArrayList<String> infix) {

        ArrayList<String> output = new ArrayList<>();
        Stack<String> st = new Stack<>();
        for (int i = 0; i < infix.size(); i++) {
            String token = infix.get(i);
            if (!isOperator(token)) {
                output.add(token);
            } else if (token.equals("(")) {
                st.push(token);
            } else if (token.equals(")")) {
                while (!st.peek().equals("(")) {
                    output.add(st.pop());
                }
                st.pop();
            } else {
                if (!st.empty() && piority(st.peek()) >= piority(token)) {
                    output.add(st.pop());
                }
                st.push(token);
            }
        }
        while (!st.isEmpty()) {
            output.add(st.pop());
        }
        return output;
    }

    private int piority(String token) {
        if (token.equals("*") || token.equals("/") || token.equals("%")) {
            return 2;
        }
        if (token.equals("+") || token.equals("-")) {
            return 1;
        }
        return 0;
    }

    private Double applyOperator(double num1, double num2, String op) {
        switch (op) {
            case "+":
                return num1 + num2;
            case "-":
                return num1 - num2;
            case "/":
                return num1 / num2;
            case "*":
                return num1 * num2;
            case "%":
                return num1 % num2;
            default:
                return null;
        }
    }

    private boolean isOperator(String token) {
        String[] operators = { "(", ")", "+", "-", "*", "/", "%" };
        for (int i = 0; i < operators.length; i++) {
            if (token.equals(operators[i])) {
                return true;
            }
        }
        return false;
    }

    public Double evaluate(Node<String> p) {
        if (p.getLeft() == null) {
            return Double.parseDouble(p.getElement());
        } else {
            Double a = evaluate(p.getLeft());
            Double b = evaluate(p.getRight());
            String op = p.getElement();
            Double result = applyOperator(a, b, op);
            return result;
        }
    }

    public void print(ArrayList<String> expession) {
        if (expession == null) {
            System.out.println("Illegal");
            return;
        }
        int i = 0;
        for (String s : expession) {
            System.out.println("Element " + i + ": " + s);
            i++;
        }
        System.out.println();
    }

    public ExpressionTree<String> generateTree(ArrayList<String> postfix) {

        Stack<Node<String>> st = new Stack<>();
        for (int i = 0; i < postfix.size(); i++) {
            String token = postfix.get(i);
            Node<String> node = new Node<String>(token, null, null, null);
            if (!isOperator(token)) {
                st.push(node);
            } else {
                node.setRight(st.pop());
                node.setLeft(st.pop());
                st.push(node);
            }
        }
        ExpressionTree<String> expTree = new ExpressionTree<>(st.pop());
        return expTree;
    }

    public static void main(String[] args) {
        CalculateExpression cal = new CalculateExpression();
        String exp = " (1 + 2 )   *   3";
        ArrayList<String> str = cal.format(exp);
        if (str == null) {
            System.out.println("Illegal Expression.");
        } else {
            ArrayList<String> postfix = cal.toPostfix(str);
            ExpressionTree<String> expTree = cal.generateTree(postfix);
            expTree.print(expTree.root(), 0);
            System.out.println("Result = " + cal.evaluate(expTree.root()));
        }

    }
}
