package hw1;

import java.util.Scanner;


class ArrayTest {

    static Scanner sc = new Scanner(System.in);

    public static <T> void output(T[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static <T> void search(T t, T[] arr) {
        String index = "";
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i].equals(t)){
                count++;
                index += String.valueOf(i) + " ";
            }
        }
        if(count != 0){
            System.out.println("Element " +"\'" + t + "\' " + "appear " + count + " times at index " + index);
        }
        else {
            System.out.println("Element " +"\'" + t + "\' " + "is not in Array.");
        }
    }

    public static <T extends Comparable<T>> void sort(T[] arr, int left, int right) {
        int i = left;
        int j = right;
        T pi = arr[(i + j) / 2];

        while (i <= j) {
            while (arr[i].compareTo(pi) < 0) {
                i++;
            }

            while (arr[j].compareTo(pi) > 0) {
                j--;
            }

            if (i <= j) {
                T t = arr[i];
                arr[i] = arr[j];
                arr[j] = t;
                i++;
                j--;
            }
        }

        if (i < right) {
            sort(arr, i, right);
        }
        if (left < j) {
            sort(arr, left, j);
        }
    }

    public static void main(final String[] args) {
        Integer[] intArr = { 1, 3, 4, 2, 6 };
        System.out.print("Array before sorting: ");
        output(intArr);
        sort(intArr, 0, intArr.length-1);
        System.out.print("Array after sorting: ");
        output(intArr);
        search(1, intArr);

        String [] strArr = {"u", "c", "h", "i", "h", "a", "i", "t", "a", "c", "h", "i"};
        System.out.print("Array before sorting: ");
        output(strArr);
        sort(strArr, 0, strArr.length-1);
        System.out.print("Array after sorting: ");
        output(strArr);
        search("c", strArr);
       


    }

}
