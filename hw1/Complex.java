package hw1;
import java.util.Scanner;

class Complex {
    private double real;
    private double imag;

    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public Complex() {

    }

    public double getReal() {
        return real;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public double getImag() {
        return imag;
    }

    public void setImag(double imag) {
        this.imag = imag;
    }

    @Override
    public String toString() {
        return this.real + " " + "+" + " " + this.imag + "i";
    }

    private static Scanner sc = new Scanner(System.in);

    public static void inputCom(Complex[] complex) {
        for (int i = 0; i < complex.length; i++) {
            System.out.print("Real " + String.valueOf(i) + ": ");
            double real = sc.nextDouble();
            System.out.print("Imag  " + String.valueOf(i) + ": ");
            double imag = sc.nextDouble();
            complex[i] = new Complex(real, imag);
        }
    }

    public static void outputCom(Complex[] complex) {
        for (int i = 0; i < complex.length; i++) {
            System.out.println(complex[i].toString());
        }
    }

    public static void sumCom(Complex[] C) {
        double real = 0;
        double imag = 0;
        for (int i = 0; i < C.length; ++i) {
            real += C[i].getReal();
            imag += C[i].getImag();
        }

        System.out.println(real + " " + "+" + " " + imag + "i");
    }

    public static Complex mul2Com(Complex A, Complex B) {
        Complex C = new Complex();
        C.setReal(A.getReal() * A.getImag() - B.getReal() * B.getImag());
        C.setImag(A.getImag() * B.getReal() + A.getReal() * B.getImag());
        return C;
    }

    public static void mulCom(Complex[] C) {
        int i = 1;

        Complex now = C[0];

        while (i < C.length) {
            now = mul2Com(now, C[i]);
            i++;
        }

        System.out.println(now.toString());
    }

    public static void main(String[] args) {
        int n = sc.nextInt();
        Complex[] complex = new Complex[n];

        inputCom(complex);
        outputCom(complex);
        System.out.println("Tong cac so phuc la: ");
        sumCom(complex);
        System.out.println("Tich cac so phuc la: ");
        mulCom(complex);

    }
}