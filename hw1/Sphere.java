package hw1;
class Sphere {
    private double x;
    private double y;
    private double z;
    private double R;

    public Sphere() {
    }

    public Sphere(double x, double y, double z, double r) {
        this.x = x;
        this.y = y;
        this.z = z;
        R = r;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getR() {
        return R;
    }

    public void setR(double r) {
        R = r;
    }

    @Override
    public String toString() {
        return "Sphere [x=" + x + ", y=" + y + ", z=" + z + ", R=" + R + "]";
    }

    public static double Sxq(Sphere S) {
        double sxq = 4 * Math.PI * Math.pow(S.getR(), 2);

        return sxq;
    }

    public static double Volume(Sphere S) {
        double volume = (4 / 3) * Math.PI * Math.pow(S.getR(), 3);

        return volume;
    }

    public static void Relation(Sphere One, Sphere Two) {
        if (Math.sqrt(Math.pow((Two.getX() - One.getX()), 2) + Math.pow((Two.getY() - One.getY()), 2)
                + Math.pow((Two.getZ() - One.getZ()), 2)) <= Math.abs(One.getR() - Two.getR())) {

            System.out.println("2 Sphere om ap nhau");
        } else if (Math.sqrt(Math.pow((Two.getX() - One.getX()), 2) + Math.pow((Two.getY() - One.getY()), 2)
                + Math.pow((Two.getZ() - One.getZ()), 2)) > Math.abs(One.getR() + Two.getR())) {

            System.out.println("2 Sphere khong den duoc voi nhau");
        } else {
            System.out.println("2 Sphere buoc qua doi nhau");
        }

    }

    public static void main(String[] args) {
        Sphere sphereOne = new Sphere(2, 3, 1, 10);
        Sphere sphereTwo = new Sphere(3, 1, 2, 8);

        System.out.println(sphereOne.toString());

        double Sxq = Sxq(sphereOne);
        double Volume = Volume(sphereOne);
        System.out.println("Sxq " + " " + "[" + Sxq + "]");
        System.out.println("Volume " + " " + "[" + Volume + "]");

        System.out.println();
        System.out.println(sphereOne.toString());
        System.out.println(sphereTwo.toString());
        Relation(sphereOne, sphereTwo);
    }
}